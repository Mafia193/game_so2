cmake_minimum_required(VERSION 3.16.3)
project(player)

set(CMAKE_CXX_STANDARD 17)

ADD_LIBRARY(LibsModule main.cpp player.h)
target_link_libraries(LibsModule -lpthread -lrt -lncursesw)

add_executable(player main.cpp)

target_link_libraries(player LibsModule)