//
// Created by Lenovo on 21.01.2021.
//

#ifndef SOUT_H
#define SOUT_H

#include "screen.h"

#undef endl
#define endl '\n'
/*------------------------------------------------------------------------------------*/
class Sout {
public:
    Sout() = default;
    ~Sout() = default;
    Sout & operator<< (const bool val);
    Sout & operator<< (const short val);
    Sout & operator<< (const unsigned short val);
    Sout & operator<< (const int val);
    Sout & operator<< (const unsigned int val);
    Sout & operator<< (const long val);
    Sout & operator<< (const unsigned long val);
    Sout & operator<< (const long long val);
    Sout & operator<< (const unsigned long long val);
    Sout & operator<< (const float val);
    Sout & operator<< (const double val);
    Sout & operator<< (const long double val);
    Sout & operator<< (const void* val);
    Sout & operator<< (const char* val);
    Sout & operator<< (const char val);
};
/*------------------------------------------------------------------------------------*/
Sout & Sout::operator<< (const bool val) {
    printw ("%B", val);
    return *this;
}

Sout & Sout::operator<< (const short val) {
    printw ("%hd", val);
    return *this;
}

Sout & Sout::operator<< (const unsigned short val) {
    printw ("%hu", val);
    return *this;
}

Sout & Sout::operator<< (const int val) {
    printw ("%d", val);
    return *this;
}

Sout & Sout::operator<< (const unsigned int val) {
    printw ("%u", val);
    return *this;
}

Sout & Sout::operator<< (const long val) {
    printw ("%ld", val);
    return *this;
}

Sout & Sout::operator<< (const unsigned long val) {
    printw ("%lu", val);
    return *this;
}

Sout & Sout::operator<< (const long long val) {
    printw ("%lld", val);
    return *this;
}

Sout & Sout::operator<< (const unsigned long long val) {
    printw ("%llu", val);
    return *this;
}

Sout & Sout::operator<< (const float val) {
    printw ("%f", val);
    return *this;
}

Sout & Sout::operator<< (const double val) {
    printw ("%f", val);
    return *this;
}

Sout & Sout::operator<< (const long double val) {
    printw ("%Lf", val);
    return *this;
}

Sout & Sout::operator<< (const void* val) {
    printw ("%p", val);
    return *this;
}

Sout & Sout::operator<< (const char* val) {
    while (*val != '\0') {
        if (*val == endl) {
            int y;
            int x;
            getyx (stdscr, y, x);
            move (y + 1, 0);
        } else {
            printw ("%c", *val);
        }
        ++val;
    }
    return *this;
}

Sout & Sout::operator<< (const char val) {
    if (val == endl) {
        int y;
        int x;
        getyx (stdscr, y, x);
        move (y + 1, 0);
    } else {
        printw ("%c", val);
    }
    return *this;
}
/*------------------------------------------------------------------------------------*/
static Sout cout;

#endif //SOUT_H
