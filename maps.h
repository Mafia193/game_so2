//
// Created by Lenovo on 22.01.2021.
//

#ifndef MAPS_H
#define MAPS_H

#include <vector>
#include <string>

#include "common.h"

using std::vector;

#define maxHight 30
#define maxWidth 60

#define ZONE 2

#define _WALL     "█"
#define _BUSH     "#"
#define _SPACE    " "
#define _CAMPSITE "A"
#define _FOG      "+"
#define _COIN     "c"
#define _TREASURE "t"
#define _LARGE_TREASURE   "T"
#define _DROPPED_TREASURE "D"

//#define GODMODE
/*---------------------------------------------------------------------------------------*/
class SmallMap {
    Object  map[5][5];
public:
    SmallMap()  = default;
    ~SmallMap() = default;
    void put (const Object &c, const Point & position);
    Object get (const Point & pos);
    void operator= (const SmallMap & m);
    void update();
};
/*---------------------------------------------------------------------------------------*/
struct Player {
    int      playerNum;
    int      PID;
    int      serverPID;
    Type     type;
    Point    currentPosition;
    Point    respawnPosition;
    int      Deaths;
    struct {
        int  carried;
        int  brought;
    } coins;
    char     ssemaphore[MESSAGE_SIZE];
    char     psemaphore[MESSAGE_SIZE];
    char     memory[MESSAGE_SIZE];
    SmallMap omap;
    SmallMap lmap;
    int      roundNum;
    Point    direction;
    bool     end;
    bool     login;
    bool     slowdown;
};
/*---------------------------------------------------------------------------------------*/
class Map {
    pthread_mutex_t     map_mutex{};
    pthread_mutexattr_t map_mutexattr{};
    Point  campsiteLocation;
    Object wall;
    Object bush;
    Object space;
    Object campsite;
    Object fog;
    vector<Player *> players;

    int Hight;
    int Width;
    Object omap[maxHight][maxWidth];
    Object map[maxHight][maxWidth];
    int round = 0;

public:
    Map() noexcept;
    explicit Map (const char* mapName);
    ~Map();
    Point choose_position();
    void paint (int playerNum);
    void add_player (Player * player, int number, int playerPID, int serverPID, Type _type);
    void add_player (Player * player, int num);
    void get_map (const int playerNum, SmallMap & oSmallMap, SmallMap & smallMap);
    void fuse_map (SmallMap & oSmallMap, SmallMap & smallMap, const Point & pos);
    void move_player (const int playerNum, const Point moveBy);
    void throw_carrot (Carrots carrot, int number);
    void inject_player (int number, struct Player * player1 ,const struct Player * player2, int gu);
    void get_player (struct Player * player);
    void update_player (const struct Player * player, int num);
    void update_round (int _round);
    void logout_player (int playerNum);
    Object get_object (const Point & position);
    int aim_player (Point & myPosition);
};
/*---------------------------------------------------------------------------------------*/
void SmallMap::put (const Object &c, const Point & position) {
    map[position.y][position.x] = c;
}

Object SmallMap::get (const Point & pos) {
    return map[pos.y][pos.x];
}

void SmallMap::operator= (const SmallMap & m) {
    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 5; ++j) {
            map[i][j] = m.map[i][j];
        }
    }
}

void SmallMap::update() {
    if (map[1][1] == _WALL) {
        map[0][0] = _WALL;
        map[0][1] = _WALL;
        map[1][0] = _WALL;
    }
    if (map[1][2] == _WALL) {
        map[0][1] = _WALL;
        map[0][2] = _WALL;
        map[0][3] = _WALL;
    }
    if (map[1][3] == _WALL) {
        map[0][3] = _WALL;
        map[0][4] = _WALL;
        map[1][4] = _WALL;
    }
    if (map[2][3] == _WALL) {
        map[1][4] = _WALL;
        map[2][4] = _WALL;
        map[3][4] = _WALL;
    }
    if (map[3][3] == _WALL) {
        map[3][4] = _WALL;
        map[4][4] = _WALL;
        map[4][3] = _WALL;
    }
    if (map[3][2] == _WALL) {
        map[4][1] = _WALL;
        map[4][2] = _WALL;
        map[4][3] = _WALL;
    }
    if (map[3][1] == _WALL) {
        map[3][0] = _WALL;
        map[4][0] = _WALL;
        map[4][1] = _WALL;
    }
    if (map[2][1] == _WALL) {
        map[1][0] = _WALL;
        map[2][0] = _WALL;
        map[3][0] = _WALL;
    }
}
/*---------------------------------------------------------------------------------------*/
Map::Map() noexcept: wall(_WALL), bush(_BUSH), space(_SPACE), campsite(_CAMPSITE), fog(_FOG) {
    pthread_mutexattr_init (&map_mutexattr);
    pthread_mutexattr_settype (&map_mutexattr, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init (&map_mutex, &map_mutexattr);

    for (int i = 0; i < maxHight; ++i) {
        for (int j = 0; j < maxWidth; ++j) {
            omap[i][j] = fog;
            map[i][j]  = fog;
        }
    }
    campsiteLocation.set (-1, -1);
    Hight = maxHight;
    Width = maxWidth;
}

Map::Map (const char* mapName) : wall(_WALL), bush(_BUSH), space(_SPACE), campsite(_CAMPSITE), fog(_FOG){
    pthread_mutexattr_init (&map_mutexattr);
    pthread_mutexattr_settype (&map_mutexattr, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init (&map_mutex, &map_mutexattr);

    char _char[3];
    cout << "otwieranie pliku" << endl;
    FILE *fmap = fopen (mapName, "r+");
    if (!fmap) {
        cout << "nie udało się otworzyć pliku" << endl;
    } else {
        cout << "plik został otwarty" << endl;
    }
    cout << "przed odczytywaniem" << endl;

    Width = 0;
    Hight = 0;
    for (int i = 0; i < maxHight; ++i) {
        for (int j = 0; j < maxWidth; ++j) {
            fread (_char, 3, 1, fmap);
            if (*_char == '\r') {
                if (i == 0) {
                    Width = j;
                }
                fseek (fmap, -1, SEEK_CUR);
                break;
            }
            omap[i][j] = _char;
            if (omap[i][j] == wall) {
                omap[i][j] = wall;
                map[i][j]  = wall;
            } else if (omap[i][j] == bush) {
                omap[i][j] = bush;
                map[i][j]  = bush;
            } else if (omap[i][j] == campsite) {
                omap[i][j] = campsite;
                map[i][j]  = campsite;
            } else {
                omap[i][j] = space;
                map[i][j]  = space;
            }
            if (feof (fmap)) {
                Hight = i + 1;
                i = maxHight;
                break;
            }
            fseek (fmap, -1 * omap[i][j].left(), SEEK_CUR);
            if (omap[i][j] == campsite) {
                campsiteLocation.set (i, j);
            }
        }
        if (Width == 0) {
            Width = maxWidth;
        }
    }
    fclose (fmap);
}

Map::~Map() {
    pthread_mutex_destroy (&map_mutex);
    pthread_mutexattr_destroy (&map_mutexattr);
    players.clear();
}

Point Map::choose_position() {
    Point point;
    pthread_mutex_lock (&map_mutex);
    do {
        point.random (Width, Hight);
    } while (map[point.y][point.x] != space);
    pthread_mutex_unlock (&map_mutex);
    return point;
}

void Map::paint (int playerNum = -1) {
    pthread_mutex_lock (&map_mutex);
    int y;
    int x;
    getyx (stdscr, y, x);
    move (0, 0);

    if (playerNum >= 0) {
        for (int i = 0; i < Hight; ++i) {
            for (int j = 0; j < Width; ++j) {
                if (i >= players[playerNum]->currentPosition.y - ZONE && i <= players[playerNum]->currentPosition.y + ZONE && j >= players[playerNum]->currentPosition.x - ZONE && j <= players[playerNum]->currentPosition.x + ZONE) {
                    cout << map[i][j];
                } else {
                    cout << omap[i][j];
                }
            }
            cout << endl;
        }
    } else {
        for (int i = 0; i < Hight; ++i) {
            for (int j = 0; j < Width; ++j) {
                cout << map[i][j];
            }
            cout << endl;
        }
    }

    int j = 0;
    int k = Width + 1;
    for (auto i : players) {
        if(i->type != MONSTER) {
            move (j++, k);
            cout << "Playe number: "     << i->playerNum;
            move (j++, k);
            cout << "Playe PID: "        << i->PID;
            move (j++, k);
            cout << "Type: "             << (i->type == HUMAN ? "HUMAN" : (i->type == CPU ? "CPU" : "MONSTER"));
            move (j++, k);
            cout << "Current position: " << i->currentPosition  << "    ";
            move (j++, k);
            cout << "Respown position: " << i->respawnPosition;
            move (j++, k);
            cout << "Coins found: "      << i->coins.carried    << "    ";
            move (j++, k);
            cout << "Coins brought: "    << i->coins.brought    << "    ";
            move (j++, k);
            cout << "Deaths: "           << i->Deaths;
            move (j++, k);
            cout << "Round: "            << i->roundNum         << "    ";
            move (j++, k);
        }
    }
    refresh();
    move (y, x);
    pthread_mutex_unlock (&map_mutex);
}

void Map::add_player (Player * player, int number, int playerPID, int serverPID, Type _type) {
    pthread_mutex_lock (&map_mutex);

    player->playerNum       = number;
    player->PID             = playerPID;
    player->serverPID       = serverPID;
    player->type            = _type;
    player->respawnPosition = choose_position();
    player->currentPosition = player->respawnPosition;
    player->Deaths          = 0;
    player->coins.carried   = 0;
    player->coins.brought   = 0;
    player->roundNum        = round;
    player->direction       = Point (0, 0);
    player->end             = false;
    player->login           = true;
    player->slowdown        = false;

    switch (_type) {
        case CPU:
        case HUMAN:
            map[player->currentPosition.y][player->currentPosition.x] = '0' + player->playerNum + 1;
            break;
        case MONSTER:
            map[player->currentPosition.y][player->currentPosition.x] = '*';
            break;
    }

    players.push_back(player);
    get_map (number, players[number]->omap, players[number]->lmap);

    pthread_mutex_unlock (&map_mutex);
}

void Map::add_player (Player * player, int num) {
    pthread_mutex_lock (&map_mutex);
    players.push_back (player);
    inject_player (num, players[num], player, 1);
    pthread_mutex_unlock (&map_mutex);
}

void Map::get_map (const int playerNum, SmallMap & oSmallMap, SmallMap & smallMap) {
    pthread_mutex_lock (&map_mutex);
    if (!players[playerNum]->login) {
        pthread_mutex_unlock (&map_mutex);
        return;
    }
    Point pos = players[playerNum]->currentPosition;
    for (int y = -2, sy = 0; y < 3; ++y, ++sy) {
        for (int x = -2, sx = 0; x < 3; ++x, ++sx) {
            if (pos.y + y < 0 || pos.y + y >= Hight || pos.x + x < 0 || pos.x + x >= Width) {
                oSmallMap.put (fog, Point (sy, sx));
                smallMap.put  (fog, Point (sy, sx));
            } else {
                oSmallMap.put (omap[pos.y + y][pos.x + x], Point (sy, sx));
                smallMap.put  (map[pos.y + y][pos.x + x], Point (sy, sx));
            }
        }
    }
    pthread_mutex_unlock (&map_mutex);
}

void Map::fuse_map (SmallMap & oSmallMap, SmallMap & smallMap, const Point & pos) {
    pthread_mutex_lock (&map_mutex);
    const int zone2 = ZONE * 2 + 1;

    for (int i = 0; i < zone2; ++i) {
        for (int j = 0; j < zone2; ++j) {
            int y = pos.y + i - ZONE;
            int x = pos.x + j - ZONE;
            if (y >= 0 && y < Hight && x >= 0 && x < Width) {
                omap[y][x] = oSmallMap.get (Point (i, j));
                map[y][x] = smallMap.get (Point (i, j));
            }
        }
    }

    pthread_mutex_unlock (&map_mutex);
}

void Map::throw_carrot (Carrots carrot, int number = 1) {
    pthread_mutex_lock (&map_mutex);
    for (int i = 0; i < number; ++i) {
        Point position = choose_position();
        switch (carrot) {
            case COIN:
                map[position.y][position.x] = Object (_COIN, COIN);
                break;
            case TREASURE:
                map[position.y][position.x] = Object (_TREASURE, TREASURE);
                break;
            case LARGE_TREASURE:
                map[position.y][position.x] = Object (_LARGE_TREASURE, LARGE_TREASURE);
                break;
            default:
                break;
        }
    }
    pthread_mutex_unlock (&map_mutex);
}

void Map::move_player (const int playerNum, const Point moveBy) {
    pthread_mutex_lock (&map_mutex);
    if (!players[playerNum]->login) {
        pthread_mutex_unlock (&map_mutex);
        return;
    }
    Point pos = players[playerNum]->currentPosition;
    if (players[playerNum]->end) {
        logout_player (playerNum);
        pthread_mutex_unlock (&map_mutex);
        return;
    }
    Point next = pos + moveBy;
    int secPlayerNum;
#ifdef GODMODE
    if (true) {
#else
    if (omap[next.y][next.x] != wall && !players[playerNum]->slowdown) {
#endif
        if (players[playerNum]->type == MONSTER) {
            int copyValue = 0;
            if (map[next.y][next.x] == _COIN) {
                copyValue = COIN;
            } else if (map[next.y][next.x] == _TREASURE) {
                copyValue = TREASURE;
            } else if (map[next.y][next.x] == _LARGE_TREASURE) {
                copyValue = LARGE_TREASURE;
            } else if (map[next.y][next.x] == _DROPPED_TREASURE) {
                copyValue = map[next.y][next.x].value;
            } else if ((secPlayerNum = map[next.y][next.x].is_player ())) {
                if (secPlayerNum != (playerNum + 1)) {
                    --secPlayerNum;
                    copyValue = players[secPlayerNum]->coins.carried;
//                    map[pos.y][pos.x] = omap[pos.y][pos.x];

                    players[secPlayerNum]->coins.carried = 0;
                    ++players[secPlayerNum]->Deaths;
                    players[secPlayerNum]->currentPosition = players[secPlayerNum]->respawnPosition;
                    players[secPlayerNum]->slowdown = false;
                    move_player (secPlayerNum, Point (0, 0)); // respawn
                }
            } else if (map[next.y][next.x] == _BUSH) {
                players[playerNum]->slowdown = true;
            }
            if (map[pos.y][pos.x].value == COIN) {
                map[pos.y][pos.x] = Object (_COIN, COIN);
            } else if (map[pos.y][pos.x].value == TREASURE) {
                map[pos.y][pos.x] = Object (_TREASURE, TREASURE);
            } else if (map[pos.y][pos.x].value == LARGE_TREASURE) {
                map[pos.y][pos.x] = Object (_LARGE_TREASURE, LARGE_TREASURE);
            } else if (map[pos.y][pos.x].value) {
                map[pos.y][pos.x] = Object (_DROPPED_TREASURE, map[pos.y][pos.x].value);
            } else {
                map[pos.y][pos.x] = omap[pos.y][pos.x];
            }
            map[next.y][next.x] = '*';
            map[next.y][next.x].value = copyValue;
            players[playerNum]->currentPosition += moveBy;
        } else {
            if (map[next.y][next.x] == _COIN) {
                players[playerNum]->coins.carried += map[next.y][next.x].value;
                throw_carrot (COIN, 1);
            } else if (map[next.y][next.x] == _TREASURE) {
                players[playerNum]->coins.carried += map[next.y][next.x].value;
                throw_carrot (TREASURE, 1);
            } else if (map[next.y][next.x] == _LARGE_TREASURE) {
                players[playerNum]->coins.carried += map[next.y][next.x].value;
                throw_carrot (LARGE_TREASURE, 1);
            } else if (map[next.y][next.x] == _DROPPED_TREASURE) {
                players[playerNum]->coins.carried += map[next.y][next.x].value;
                throw_carrot (DROPPED_TREASURE, 1);
            } else if (map[next.y][next.x] == _CAMPSITE) {
                players[playerNum]->coins.brought += players[playerNum]->coins.carried;
                players[playerNum]->coins.carried = 0;
            } else if ((secPlayerNum = map[next.y][next.x].is_player ())) {
                if (secPlayerNum != (playerNum + 1)) {
                    --secPlayerNum;
                    map[next.y][next.x] = Object (_DROPPED_TREASURE, players[playerNum]->coins.carried +
                                                                     players[secPlayerNum]->coins.carried);
                    map[pos.y][pos.x] = omap[pos.y][pos.x];

                    players[playerNum]->coins.carried = 0;
                    ++players[playerNum]->Deaths;
                    players[playerNum]->currentPosition = players[playerNum]->respawnPosition;
                    move_player (playerNum, Point (0, 0)); // respawn

                    players[secPlayerNum]->coins.carried = 0;
                    ++players[secPlayerNum]->Deaths;
                    players[secPlayerNum]->currentPosition = players[secPlayerNum]->respawnPosition;
                    players[secPlayerNum]->slowdown = false;
                    move_player (secPlayerNum, Point (0, 0)); // respawn

                    players[playerNum]->direction = Point (0, 0);
                    pthread_mutex_unlock (&map_mutex);
                    return;
                }
            } else if (map[next.y][next.x].is_monster ()) {
                map[pos.y][pos.x] = Object (_DROPPED_TREASURE, players[playerNum]->coins.carried);
                players[playerNum]->coins.carried = 0;
                ++players[playerNum]->Deaths;
                players[playerNum]->currentPosition = players[playerNum]->respawnPosition;
                move_player (playerNum, Point (0, 0)); // respawn
                players[playerNum]->direction = Point (0, 0);
                pthread_mutex_unlock (&map_mutex);
                return;
            } else if (map[next.y][next.x] == _BUSH) {
                players[playerNum]->slowdown = true;
            }
            map[pos.y][pos.x] = omap[pos.y][pos.x];
            map[next.y][next.x] = '0' + playerNum + 1;
            players[playerNum]->currentPosition += moveBy;
        }
    } else if (players[playerNum]->slowdown) {
        players[playerNum]->slowdown = false;
    }
    players[playerNum]->direction = Point (0, 0);
    pthread_mutex_unlock (&map_mutex);
    return;
}

void Map::inject_player (int number, struct Player * player1 ,const struct Player * player2, int gu) {
    pthread_mutex_lock (&map_mutex);
    player1->playerNum       = player2->playerNum;
    player1->PID             = player2->PID;
    player1->serverPID       = player2->serverPID;
    player1->type            = player2->type;
    player1->currentPosition = player2->currentPosition;
    player1->respawnPosition = player2->respawnPosition;
    player1->Deaths          = player2->Deaths;
    player1->coins.carried   = player2->coins.carried;
    player1->coins.brought   = player2->coins.brought;

    for (int i = 0; i < MESSAGE_SIZE; ++i) {
        player1->ssemaphore[i] = player2->ssemaphore[i];
        player1->psemaphore[i] = player2->psemaphore[i];
        player1->memory[i]     = player2->memory[i];
    }

    if (gu) {
        fuse_map (player1->omap, player1->lmap, player1->currentPosition);
    } else {
        get_map (number, player1->omap, player1->lmap);
    }

    player1->roundNum  = player2->roundNum;
    player1->direction = player2->direction;
    player1->end       = player2->end;
    player1->login     = player2->login;
    player1->slowdown  = player2->slowdown;
    pthread_mutex_unlock (&map_mutex);
}

void Map::get_player (struct Player * player) {
    pthread_mutex_lock (&map_mutex);
    inject_player (player->playerNum, player, players[player->playerNum], 0);
    pthread_mutex_unlock (&map_mutex);
}

void Map::update_player (const struct Player * player, int num) {
    pthread_mutex_lock (&map_mutex);
    players[num]->currentPosition = player->currentPosition;
    players[num]->Deaths          = player->Deaths;
    players[num]->coins.carried   = player->coins.carried;
    players[num]->coins.brought   = player->coins.brought;
    players[num]->roundNum        = player->roundNum;
    players[num]->login           = player->login;
    players[num]->slowdown        = player->slowdown;

    fuse_map (players[num]->omap, players[num]->lmap, players[num]->currentPosition);
    pthread_mutex_unlock (&map_mutex);
}

void Map::update_round (int _round) {
    pthread_mutex_lock (&map_mutex);
    round = _round;
    for (auto player : players) {
        player->roundNum = _round;
    }
    pthread_mutex_unlock (&map_mutex);
}

void Map::logout_player (int playerNum) {
    pthread_mutex_lock (&map_mutex);
    Point pos = players[playerNum]->currentPosition;
    players[playerNum]->end = true;
    map[pos.y][pos.x] = omap[pos.y][pos.x];
    players[playerNum]->login = false;
    pthread_mutex_unlock (&map_mutex);
}

Object Map::get_object (const Point & position) {
    if (position.y >=0 && position.y < Hight && position.x >=0 && position.y < Width) {
        return map[position.y][position.x];
    } else {
        return Object();
    }
}

int Map::aim_player (Point & myPosition) {
    int direction = -1;
    for (int i = -2; i <= 2; ++i) {
        for (int j = -2; j <= 2; ++j) {
            if (map[myPosition.y + i][myPosition.x + j].is_player()) {
                if (j < 0 && map[myPosition.y][myPosition.x - 1] != _WALL) {
                    direction = 3;
                } else if (j > 0 && map[myPosition.y][myPosition.x + 1] != _WALL) {
                    direction = 1;
                }
                if (i < 0 && map[myPosition.y - 1][myPosition.x] != _WALL) {
                    direction = 0;
                } else if (i > 0 && map[myPosition.y + 1][myPosition.x] != _WALL) {
                    direction = 2;
                }
                return direction;
            }
        }
    }
    return direction;
}
/*---------------------------------------------------------------------------------------*/

#endif //MAPS_H
