//
// Created by Lenovo on 09.01.2021.
//

#ifndef GAME_COMMON_H
#define GAME_COMMON_H

#include <cstdio>
#include <cstdlib>
#include <semaphore.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/mman.h>
#include <unistd.h>

#include "point.h"

//semafors
#define SERVER_ON    (char*)"so2_game_server_on"
#define SERVER_ON2   (char*)"so2_game_server_on2"
#define PLAYER_JOIN  (char*)"so2_game_player_join"
#define PLAYER_JOIN2 (char*)"so2_game_player_join2"
#define GAME_START   (char*)"so2_game_game_start"
#define PLAYERX_SEM  (char*)"so2_game_psem_"          //"so2_game_psem_PID"
#define SERVERX_SEM  (char*)"so2_game_ssem_"          //"so2_game_ssem_PID"

#define MEMORY       (char*)"so2_game_memory"
#define PLAYERX_MEM  (char*)"so2_game_mem_"          //"so2_game_mem_PID"
#define SHM_FAILED  -1

#define MESSAGE_SIZE 32
//////////

enum Type {CPU, HUMAN, MONSTER};
enum Carrots {COIN = 1, TREASURE = 10, LARGE_TREASURE = 50, DROPPED_TREASURE = 0};

/*------------------------------------------------------------------------------*/
class SEMAPHORE {
    char  *sem_name;
    int   sem_new;
    sem_t *sem;
public:
    SEMAPHORE () noexcept;
    SEMAPHORE (char *name, int _sem_new);
    ~SEMAPHORE ();
    SEMAPHORE (const SEMAPHORE & semaphore) = delete;
    SEMAPHORE & operator= (const SEMAPHORE & semaphore) = delete;
    SEMAPHORE & operator= (SEMAPHORE && semaphore);
    SEMAPHORE (SEMAPHORE && semaphore);
    SEMAPHORE & open (char *name, int _sem_new);
    int post ();
    int wait ();
    int trywait ();
    int timedwait (struct timespec &ts);
    bool operator== (sem_t* sem1);
    void unlink ();
};
/*------------------------------------------------------------------------------*/
class SHM {
    char *shm_name;
    int  shm_size;
    void *ptr;
    int  shm_new;
    int  fd;
public:
    SHM() noexcept;
    SHM(char* _name, int _size, void** _ptr, int _new);
    ~SHM();
    SHM (const SHM & shm) = delete;
    SHM & operator= (const SHM & shm) = delete;
    SHM & operator= (SHM && shm);
    SHM (SHM && shm);
    SHM & open (char* _name, int _size, void** _ptr, int _new);
    bool operator== (int i);
    void unlink ();
};
/*---------------------------------------------------------------------------------*/
class Object {
    char c[3];
public:
    int value;
    Object();
    Object(const char* _char, int _value);
    ~Object() = default;
    bool operator== (const char* _char);
    bool operator!= (const char* _char);
    bool operator== (const Object _char);
    bool operator!= (const Object _char);
    void operator= (const char s);
    void operator= (const char *s);
    int left ();
    int is_player ();
    bool is_monster ();
    friend Sout & operator<< (Sout &out, const Object & myChar) {
        for (int i = 0; i < 3; ++i) {
            if (myChar.c[i] == '\0') {
                break;
            }
            out << myChar.c[i];
        }
        return out;
    }
};
/*---------------------------------------------------------------------------------*/
SEMAPHORE::SEMAPHORE () noexcept : sem_name(nullptr), sem_new(0), sem(NULL) {};

SEMAPHORE::SEMAPHORE (char *name, int _sem_new) : sem_name(name), sem_new(_sem_new) {
    if (sem_new) {
        sem_unlink (sem_name);
        sem = sem_open (sem_name, O_CREAT | O_EXCL, 0600, 0);
    } else {
        sem = sem_open (sem_name, 0);
    }
    if (sem == SEM_FAILED) {
        perror ("sem_open");
        throw (0);
    }
}

SEMAPHORE::~SEMAPHORE () {
    sem_close (sem);
    if (sem_new) {
        sem_unlink (sem_name);
    }
}

SEMAPHORE & SEMAPHORE::operator= (SEMAPHORE && semaphore)  {
    if (this == &semaphore) {
        return *this;
    }
    sem_name = semaphore.sem_name;
    sem_new = semaphore.sem_new;
    sem = semaphore.sem;

    semaphore.sem_name = nullptr;
    semaphore.sem_new = 0;
    semaphore.sem = nullptr;
    return *this;
}

SEMAPHORE::SEMAPHORE (SEMAPHORE && semaphore) {
    sem_name = semaphore.sem_name;
    sem_new = semaphore.sem_new;
    sem = semaphore.sem;

    semaphore.sem_name = nullptr;
    semaphore.sem_new = 0;
    semaphore.sem = nullptr;
}

SEMAPHORE & SEMAPHORE::open (char *name, int _sem_new) {
    sem_name = name;
    sem_new  = _sem_new;
    if (sem_new) {
        sem_unlink (sem_name);
        sem = sem_open (sem_name, O_CREAT | O_EXCL, 0600, 0);
    } else {
        sem = sem_open (sem_name, 0);
    }
    if (sem == SEM_FAILED) {
        perror("sem_open");
        throw (0);
    }
    return *this;
}

int SEMAPHORE::post() {
    return sem_post (sem);
}

int SEMAPHORE::wait() {
    return sem_wait (sem);
}

int SEMAPHORE::trywait() {
    return sem_trywait (sem);
}

int SEMAPHORE::timedwait(struct timespec &ts) {
    return sem_timedwait (sem, &ts);
}

bool SEMAPHORE::operator== (sem_t* sem1) {
    return (sem == sem1);
}

void SEMAPHORE::unlink () {
    sem_unlink (sem_name);
}
/*------------------------------------------------------------------------------------*/
SHM::SHM() noexcept: shm_name(nullptr), shm_size(0), ptr(nullptr), shm_new(0), fd(0) {};

SHM::SHM(char* _name, int _size, void** _ptr, int _new) : shm_name(_name), shm_size(_size), shm_new(_new) {
    if (shm_new) {
        fd = shm_open (shm_name, O_CREAT | O_RDWR | O_EXCL, 0600);
    } else {
        fd = shm_open (shm_name, O_RDWR, 0600);
    }
    if (fd == SHM_FAILED) {
        perror ("shm_open");
        throw (0);
    }
    int status = ftruncate (fd, shm_size);
    if (status == SHM_FAILED) {
        perror ("ftruncate");
        throw (1);
    }
    ptr = mmap (NULL, shm_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (ptr == MAP_FAILED) {
        perror ("mmap");
        throw (2);
    }
    *_ptr = ptr;
}

SHM::~SHM() {
    munmap (ptr, shm_size);
    if (fd) {
        close (fd);
    }
    if (shm_new) {
        shm_unlink (shm_name);
    }
}

SHM & SHM::operator= (SHM && shm) {
    if (this == &shm) {
        return *this;
    }

    shm_name = shm.shm_name;
    shm_size = shm.shm_size;
    ptr = shm.ptr;
    shm_new = shm.shm_new;
    fd = shm.fd;

    shm.shm_name = nullptr;
    shm.shm_size = 0;
    shm.ptr = nullptr;
    shm.shm_new = 0;
    shm.fd = 0;

    return *this;
}

SHM::SHM (SHM && shm) {
    shm_name = shm.shm_name;
    shm_size = shm.shm_size;
    ptr = shm.ptr;
    shm_new = shm.shm_new;
    fd = shm.fd;

    shm.shm_name = nullptr;
    shm.shm_size = 0;
    shm.ptr = nullptr;
    shm.shm_new = 0;
    shm.fd = 0;
};

SHM & SHM::open (char* _name, int _size, void** _ptr, int _new) {
    shm_name = _name;
    shm_size = _size;
    shm_new = _new;
    if (shm_new) {
        fd = shm_open (shm_name, O_CREAT | O_RDWR | O_EXCL, 0600);
    } else {
        fd = shm_open (shm_name, O_RDWR, 0600);
    }
    if (fd == SHM_FAILED) {
        perror ("shm_open");
        throw (0);
    }
    int status = ftruncate (fd, shm_size);
    if (status == SHM_FAILED) {
        perror ("ftruncate");
        throw (1);
    }
    ptr = mmap (NULL, shm_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (ptr == MAP_FAILED) {
        perror ("mmap");
        throw (2);
    }
    *_ptr = ptr;
    return *this;
}

bool SHM::operator== (int i) {
    return (fd == i);
}

void SHM::unlink () {
    shm_unlink (shm_name);
}
/*---------------------------------------------------------------------------------------*/
Object::Object() : value(0) {
    c[0] = '\0';
}

Object::Object(const char* _char, int _value = 0) : value(_value) {
    int i = 0;
    while (_char && (*_char != '\0' && i < 3)) {
        c[i] = _char[i];
        ++i;
    }
    if (!i) {
        c[i] = '\0';
    }
}

bool Object::operator== (const char* _char) {
    int i = 0;
    while (*_char != '\0' && c[i] != '\0' && i < 3) {
        if (c[i++] != *_char++) {
            return false;
        }
    }
    return true;
}

bool Object::operator!= (const char* _char) {
    if (this->operator== (_char)) {
        return false;
    }
    return true;
}

bool Object::operator== (const Object _char) {
    int i = 0;
    while (_char.c[i] != '\0' && c[i] != '\0' && i < 3) {
        if (c[i] != _char.c[i]) {
            return false;
        }
        ++i;
    }
    return true;
}

bool Object::operator!= (const Object _char) {
    if (this->operator== (_char)) {
        return false;
    }
    return true;
}

void Object::operator= (const char s) {
    c[0] = s;
    c[1] = '\0';
    value = 0;
}

void Object::operator= (const char *s) {
    for (int i = 0; i < 3; ++i) {
        c[i] = *(s + i);
        if (c[i] == '\0')
            break;
    }
    value = 0;
}

int Object::left () {
    int i = 0;
    for (; i < 3; ++i) {
        if (c[i] == '\0') {
            break;
        }
    }
    return 3 - i;
}

int Object::is_player () {
    return atoi(c);
}

bool Object::is_monster () {
    return *c == '*' ? true : false;
}

#endif //GAME_COMMON_H
