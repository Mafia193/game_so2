//
// Created by Lenovo on 21.01.2021.
//

#ifndef SCREEN_H
#define SCREEN_H

#include <locale>
#include <ncurses/ncurses.h>

#undef endl
#define endl '\n'
/*------------------------------------------------------------------------------------------*/
class Screen {
public:
    Screen();
    ~Screen();
};
/*------------------------------------------------------------------------------------------*/
Screen::Screen() {
    setlocale(LC_ALL, "");
    initscr();
    clear();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
    move (25,0);
}

Screen::~Screen() {
//    getch();
    clrtoeol();
    refresh();
    endwin();
}
#endif //SCREEN_H