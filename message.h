//
// Created by Lenovo on 22.01.2021.
//

#ifndef MESSAGE_H
#define MESSAGE_H

#include "maps.h"

/*-----------------------------------------------------------------------------------------*/
struct link_message {
    int     serverPID;
    int     playerPID;
    Type    type;
    char    ssemaphore[MESSAGE_SIZE];
    char    psemaphore[MESSAGE_SIZE];
    char    memory[MESSAGE_SIZE];
};
/*-----------------------------------------------------------------------------------------*/
class Message {
public:
    vector<struct Player*> players;
    vector<SEMAPHORE> ssem;
    vector<SEMAPHORE> psem;
    vector<SHM>       shm;
    int               playersNum;

    Message (const Message & message) = delete;
    Message() = default;
    ~Message();
    void push (char* ssem_name, char* psem_name, char* shm_name, Player** player, int is_new);
};
/*-----------------------------------------------------------------------------------------*/
Message::~Message() {
    players.clear();
    ssem.clear();
    psem.clear();
    shm.clear();
    --playersNum;
}

void Message::push (char* ssem_name, char* psem_name, char* shm_name, Player** player, int is_new) {
    SEMAPHORE newSSem;
    SEMAPHORE newPSem;
    SHM       newSHM;

    try {
        newSSem = std::move (SEMAPHORE (ssem_name,is_new));
        newPSem = std::move (SEMAPHORE (psem_name,is_new));
        newSHM  = std::move (SHM (shm_name, sizeof (struct Player), (void**) player, is_new));
    } catch (...) {
        throw (0);
    }

    for (int i = 0; i < MESSAGE_SIZE; ++i) {
        (*player)->ssemaphore[i] = ssem_name[i];
        (*player)->psemaphore[i] = psem_name[i];
        (*player)->memory[i]     = shm_name[i];
    }

    try{
        players.push_back (*player);
    } catch (...) {
        throw (1);
    }
    try {
        ssem.push_back (std::move (newSSem));
    } catch (...) {
        players.pop_back();
        throw (2);
    }
    try {
        psem.push_back (std::move (newPSem));
    } catch (...) {
        players.pop_back();
        ssem.pop_back();
        throw (3);
    }
    try {
        shm.push_back  (std::move (newSHM));
    } catch (...) {
        players.pop_back();
        ssem.pop_back();
        psem.pop_back();
        throw (4);
    }
    ++playersNum;
}

#endif //MESSAGE_H
