#include <cstdlib>
#include <ctime>
#include "server.h"
#include <csignal>

int PID;
Map *map;
int gameOver = false;

SEMAPHORE sem_server;
SEMAPHORE sem_server2;
SEMAPHORE sem_player;
SEMAPHORE sem_player2;

sem_t semClock;
sem_t semMove;
sem_t semPaint;

//SEMAPHORE sem_game_start;
SHM Lmem;

struct link_message *lmem;

Message message;
int round;
int action = 0;
int numOfPlayers = 0;

void* add_player(void* arg) {
    int *playersNum = (int *) arg;
    for (int i = 0; i < *playersNum; ++i) {
        srand (time (nullptr) * time (nullptr));

        sem_server.post();
        sem_player.wait();

        snprintf (lmem->ssemaphore, MESSAGE_SIZE, "%s%d", PLAYERX_SEM, lmem->playerPID);
        snprintf (lmem->psemaphore, MESSAGE_SIZE, "%s%d", SERVERX_SEM, lmem->playerPID);
        snprintf (lmem->memory,     MESSAGE_SIZE, "%s%d", PLAYERX_MEM, lmem->playerPID);

        Player *player;
        shm_unlink (lmem->memory);
        try {
            message.push (lmem->ssemaphore, lmem->psemaphore, lmem->memory, &player, 1);
        } catch (...) {
            cout << "błąd połaczenia" << endl;
            return (void*)-1;
        }
        sem_server2.post();
        map->add_player (player, numOfPlayers, lmem->playerPID, PID, lmem->type);
        message.ssem[numOfPlayers].post();

        sem_player2.wait();
        lmem->playerPID = 0;
        lmem->ssemaphore[0] = '\0';
        lmem->psemaphore[0] = '\0';
        lmem->memory[0]     = '\0';
        ++numOfPlayers;
//        cout << "gracz " << numOfPlayers + 1 << " dołączył do gry" << endl;
        map->paint();
    }
    return nullptr;
}

void handle_event () {
    switch (action) {
        case -1:
        case 0:
            return;
        case 'q':
        case 'Q':
            gameOver = true;
            break;
        case 'b':
        case 'B':
        {
            int i = 1;
            pthread_t beast;
            pthread_create (&beast, nullptr, add_player, (void*)&i);
            if (fork () == 0) {
                execl ("../../monster/cmake-build-wsl_profile/monster", nullptr);
            }
            pthread_join (beast, nullptr);
        }
            break;
        case 'c':
            map->throw_carrot(COIN);
            break;
        case 't':
            map->throw_carrot(TREASURE);
            break;
        case 'T':
            map->throw_carrot(LARGE_TREASURE);
            break;
        default:
            break;
    }
    action = 0;
}

void* handle_player(void* arg) {
    while (true) {
        sem_wait (&semClock);
        bool over = true;
        for (auto player : message.players) {
            int memStatus = kill (player->PID, 0);
            if (memStatus) {
                map->logout_player (player->playerNum);
            }
        }
        for (auto player : message.players) {
            if (player->login) {
                over = false;
                break;
            }
        }
        if (over) {
            gameOver = true;
            return nullptr;
        }
        for (int i = 0; i < message.playersNum; ++i) {
            if (!message.psem[i].trywait()) {
                map->move_player (message.players[i]->playerNum, message.players[i]->direction);
            }
        }
        handle_event ();
        sem_post (&semMove);
        map->update_round (round);
        for (int i = 0; i < message.playersNum; ++i) {
            map->get_player(message.players[i]);
            map->get_map (i, message.players[i]->omap, message.players[i]->lmap);
        }
        for (int i = 0; i < message.playersNum; ++i) {
            message.ssem[i].post();
        }
        if (gameOver) {
            return nullptr;
        }
    }
}

[[noreturn]] void* clock(void* arg) {
    round = 0;
    while (true) {
        usleep (200000);
        sem_wait (&semPaint);
        ++round;
        sem_post (&semClock);
    }
}

[[noreturn]] void* paint(void* arg) {
    while (true) {
        sem_wait (&semMove);
        map->paint();
        sem_post (&semPaint);
    }
}

[[noreturn]] void* get_event(void* arg) {
    while (true) {
        action = getch();
    }
}

int main() {
    Screen screen;
    Map mapa("../../map2.txt");
    map = &mapa;

    PID = getpid();
    cout << "PID serwera: " << PID << endl;

    try {
        Lmem.open (MEMORY, sizeof (struct link_message), (void**) & lmem, 1);
    } catch (int err) {
        if (err == 0) {
            try {
                cout << "Nie udało się otworzyć pamięci. Podejmuję kolejną próbę" << endl;
                Lmem.open (MEMORY, sizeof (struct link_message), (void**) & lmem, 0);
            } catch (int err) {
                cout << "Problem z pamięcią współdzieloną. Numer błędu: " << err << endl;
                return 1;
            }
            if (lmem->serverPID != 0) {
                int memStatus = kill (lmem->serverPID, 0);
                if (memStatus == 0) {
                    cout << "Na tym komputerze już istnieje jeden serwer" << endl;
                    cout << "Włączenie kolejnego serwera spowoduje zamknięcie poprzedniego" << endl;
                    cout << "czy kontynuować otwieranie serwera? y=yes else=no" << endl;
                    if (getch() == 'y') {
                        kill (lmem->serverPID, SIGKILL);
                    } else {
                        return 0;
                    }
                }
            }
            Lmem.unlink();
            try {
                Lmem.open (MEMORY, sizeof (struct link_message), (void**) & lmem, 1);
                cout << "Pomyślnie otwarto pamięć współdzieloną" << endl;
            } catch (int err) {
                cout << "Problem z pamięcią współdzieloną. Numer błędu: " << err << endl;
                return 1;
            }
        }
    }
    lmem->serverPID = PID;

    sem_close (&semClock);
    sem_close (&semMove);
    sem_close (&semPaint);
    try {
        sem_server.open  (SERVER_ON, 1);
        sem_server2.open (SERVER_ON2, 1);
        sem_player.open  (PLAYER_JOIN, 1);
        sem_player2.open (PLAYER_JOIN2, 1);
        sem_init (&semClock, 1, 0);
        sem_init (&semMove,  1, 0);
        sem_init (&semPaint, 1, 1);
//        sem_game_start.open(GAME_START, 1);
    } catch (...) {
        cout << "nie udało się utworzyć semaforów" << endl;
        return 1;
    }

    map->paint ();

    map->throw_carrot (COIN, 14);
    map->throw_carrot (TREASURE, 1);
    map->throw_carrot (LARGE_TREASURE, 2);
    refresh();

    int playersNum = 1;
    int CPUNum = PLAYER_MAX - playersNum;
    int monsterNum = 1;
    pthread_t thread_id;

    pthread_create (&thread_id, nullptr, add_player, &playersNum);
    pthread_join   (thread_id, nullptr);

    pthread_create (&thread_id, nullptr, add_player, &CPUNum);
    for (int i = 0; i < CPUNum; ++i) {
        if ( fork() == 0 ) {
            execl( "../../bot/cmake-build-wsl_profile/bot", nullptr );
        }
    }
    pthread_join   (thread_id, nullptr);

    pthread_create (&thread_id, nullptr, add_player, &monsterNum);
    for (int i = 0; i < monsterNum; ++i) {
        if (fork () == 0) {
            execl ("../../monster/cmake-build-wsl_profile/monster", nullptr);
        }
    }
    pthread_join   (thread_id, nullptr);

    cout << "wszyscy gracze dołączyli do gry" << endl;

    round = 0;
    pthread_t gameClock;
    pthread_create (&gameClock, nullptr, clock, nullptr);

    pthread_t getEvent;
    pthread_create (&getEvent, nullptr, get_event, nullptr);

    pthread_t handlePlayers;
    pthread_create (&handlePlayers, nullptr, handle_player, nullptr);

    pthread_t gamePaint;
    pthread_create (&gamePaint, nullptr, paint, nullptr);

    pthread_join (handlePlayers, nullptr);
    cout << "wyłączanie servera" << endl;
    return 0;
}
