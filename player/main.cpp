#include "player.h"
#include <cstring>
#include <csignal>

SEMAPHORE sem_server;
SEMAPHORE sem_server2;
SEMAPHORE sem_player;
SEMAPHORE sem_player2;
SHM Lmem;

sem_t semmove;
sem_t sempaint;
bool wasMove = false;

Map map;
Player *player;
int gameOver = false;

SEMAPHORE ssem;
SEMAPHORE psem;
SHM Pmem;

struct link_message *lmem;

void* paint (void*) {
    struct timespec ts{};
    while (true) {
        if (clock_gettime (CLOCK_REALTIME, & ts) == -1) {
            perror ("clock_gettime");
            exit (EXIT_FAILURE);
        }
        ts.tv_sec += 3;
        while (ssem.timedwait(ts) == -1) {
            int memStatus = kill (player->serverPID, 0);
            if (memStatus) {
                cout << "zerwano połączenie z serwerem" << endl;
                gameOver = true;
                sem_post (&sempaint);
                return nullptr;
            }
            ts.tv_sec += 3;
        }
        map.update_player (player, 0);
        map.paint(0);
        if (wasMove) {
            wasMove = false;
            sem_post (&sempaint);
        }
    }
}

int main() {
    Screen screen;
    map.paint();

    sem_close (&semmove);
    sem_close (&sempaint);
    try {
        sem_server.open  (SERVER_ON, 0);
        sem_server2.open (SERVER_ON2, 0);
        sem_player.open  (PLAYER_JOIN, 0);
        sem_player2.open (PLAYER_JOIN2, 0);
        sem_init (&semmove, 1, 0);
        sem_init (&sempaint, 1, 0);
    } catch (...) {
        cout << "serwer niedostępny" << endl;
        return 1;
    }

    try {
        Lmem.open (MEMORY, sizeof (struct link_message), (void**) & lmem, 0);
    } catch (...) {
        cout << "nie udało się otworzyc pamięci współdzielonej" << endl;
        return 1;
    }

    int serverStatus = kill (lmem->serverPID, 0);
    if (serverStatus == -1) {
        cout << "brak dostępnych serwerów" << endl;
        return 1;
    }

    sem_server.wait();
    lmem->playerPID = getpid();
    lmem->type = HUMAN;
    sem_player.post();
    sem_server2.wait();

    Player connection;

    mempcpy (connection.ssemaphore, lmem->ssemaphore, MESSAGE_SIZE);
    mempcpy (connection.psemaphore, lmem->psemaphore, MESSAGE_SIZE);
    mempcpy (connection.memory,     lmem->memory,     MESSAGE_SIZE);
    sem_player2.post();

    try {
        ssem.open (connection.ssemaphore, 0);
        psem.open (connection.psemaphore, 0);
        Pmem.open (connection.memory, sizeof (struct Player), (void**) & player, 0);
    } catch (...) {
        cout << "błąd przy tworzeniu semafora lub pamięci wewnętrznej" << endl;
        return 1;
    }

    ssem.wait();

    map.add_player (player, 0);
    map.paint(0);

    pthread_t gamePaint;
    pthread_create (&gamePaint, nullptr, paint, nullptr);

    while (true) {
        int post = 1;
        player->direction = Point (0,  0);
        int c = getch();
        if (gameOver) {
            break;
        }
        if (c == 'q' || c == 'Q') {
            player->end  = true;
            psem.post();
            break;
        }
        player->end  = false;
        switch (c) {
            case KEY_UP:
                player->direction = Point (-1,  0);
                break;
            case KEY_DOWN:
                player->direction = Point ( 1,  0);
                break;
            case KEY_LEFT:
                player->direction = Point ( 0, -1);
                break;
            case KEY_RIGHT:
                player->direction = Point ( 0,  1);
                break;
            default:
                post = 0;
                break;
        }
        if (post) {
            psem.post();
            wasMove = true;
            sem_wait (&sempaint);
            flushinp();
        }
    }
    move (30, 0);
    cout << "wyłączanie klienta gracza" << endl;

    return 0;
}
