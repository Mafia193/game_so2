cmake_minimum_required(VERSION 3.16.3)
project(server)

set(CMAKE_CXX_STANDARD 17)

ADD_LIBRARY(LibsModule main.cpp server.h)
target_link_libraries(LibsModule -lpthread -lrt -lncursesw)

add_executable(server main.cpp)

target_link_libraries(server LibsModule)
